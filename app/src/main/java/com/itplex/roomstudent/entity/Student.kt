package com.itplex.roomstudent.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "student_table")

class Student (
    @PrimaryKey @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "first_name") val first_name: String,
    @ColumnInfo(name = "email") val email: String,
    @ColumnInfo(name = "genre") val genre: String,
    @ColumnInfo(name = "level") val niveau: String,
    @ColumnInfo(name = "faculty") val faculty: String,
    @ColumnInfo(name = "birth_date") val birth_date: String,
    @ColumnInfo(name = "contact_1") val contact_1: String,
    @ColumnInfo(name = "contact_2") val contact_2: String?
) {

}