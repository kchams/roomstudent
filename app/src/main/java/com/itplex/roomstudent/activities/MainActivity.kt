package com.itplex.roomstudent.activities

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.itplex.roomstudent.R
import com.itplex.roomstudent.adapter.StudentAdapter
import com.itplex.roomstudent.entity.Student
import com.itplex.roomstudent.view_model.StudentViewModel

import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var studentViewModel: StudentViewModel
    companion object {
        //        var students = ArrayList<Student>()
        const val STUDENT = "student"
        //        const val STUDENTS = "students"
//        private const val addStudentActivityRequestCode = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
//        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val recyclerView = findViewById<RecyclerView>(R.id.student_list)
        var student: Student? = null
        val adapter = StudentAdapter(this@MainActivity)
        /*if ( studentViewModel.allStudents.) {
            Toast.makeText(this@MainActivity, getString(R.string.no_students), Toast.LENGTH_LONG).show()
        }else{
            recyclerView.adapter = adapter
            recyclerView.layoutManager = LinearLayoutManager(this)
        }*/
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(this@MainActivity)

        studentViewModel = ViewModelProvider(this@MainActivity).get(StudentViewModel::class.java)
//        if( studentViewModel.allStudents.value != emptyList<Student>()){
            studentViewModel.allStudents.observe(this@MainActivity, Observer { students ->
                // Update the cached copy of the students in the adapter.
                students?.let { adapter.setStudents(it) }
                Toast.makeText(this@MainActivity, "I am in setStudents of adapter", Toast.LENGTH_LONG).show()
            })
//        }

        if (intent.hasExtra(MainActivity.STUDENT)) {
            student = Gson().fromJson(intent.getStringExtra(MainActivity.STUDENT), Student::class.java)
            student?.let{
                studentViewModel.insert(student)
            }
            Toast.makeText(this@MainActivity, "Student successfully registered", Toast.LENGTH_LONG).show()
        }else {
            Toast.makeText(this@MainActivity, getString(R.string.nothing_save), Toast.LENGTH_LONG).show()
        }

        fab.setOnClickListener { view ->
            startActivity(Intent(this@MainActivity, AddAstudentActivity::class.java))
        }
    }
  /*  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == addStudentActivityRequestCode && resultCode == Activity.RESULT_OK) {
            val student = Gson().fromJson(intent.getStringExtra(STUDENT), Student::class.java)
            student?.let{
                studentViewModel.insert(student)
            }
            Toast.makeText(this@MainActivity, "Student successfully registered", Toast.LENGTH_LONG).show()
        } else {
           Toast.makeText(this@MainActivity, getString(R.string.nothing_save), Toast.LENGTH_LONG).show()
        }
    }*/
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
// Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}
