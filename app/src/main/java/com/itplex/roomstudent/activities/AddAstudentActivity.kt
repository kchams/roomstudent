package com.itplex.roomstudent.activities

import android.app.Activity
import android.app.DatePickerDialog
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Patterns
import android.view.View
import android.widget.*
import androidx.appcompat.app.AlertDialog
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.itplex.roomstudent.R
import com.itplex.roomstudent.entity.Student
import kotlinx.android.synthetic.main.activity_add_astudent.*
import java.text.SimpleDateFormat
import java.util.*

class AddAstudentActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {
    var name : String = ""
    var firstname : String = ""
    var email : String = ""
    var genre : String = ""
    var level: String = ""
    var faculty: String = ""
    var contact_1: String = ""
    var contact_2: String = ""
    var birth_date: String = ""
    var calendar = Calendar.getInstance()

    val levels = arrayOf("1ère Année", "2ème Année", "3ème Année")
    val faculties = arrayOf("Développement", "Réseaux", "Maintenance", "Genie logiciel")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_astudent)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        txt_birth_date!!.text = getString(R.string.format_date)
        make_date_spicker(findViewById(R.id.btn_date_naiss))
        spinner_niveau()
        spinner_filiere()
       /* setupFloatingLabelError(name_input_layout as TextInputLayout)
        setupFloatingLabelError(firstname_input_layout as TextInputLayout)*/

        btn_save.setOnClickListener {
            confirmation()
        }
        btn_clear.setOnClickListener {
            refresh_fields()
        }
    }

    // inputs Validate
    fun is_inputs_validate(): Boolean {
        var valid = true
        val id = group_radio_genre?.checkedRadioButtonId
        name =  edt_name.text.toString()
        firstname = edt_firstname.text.toString()
        email = edt_email.text.toString()
        level = sp_niveau.selectedItem.toString()
        faculty = sp_filiere.selectedItem.toString()
        contact_1 = edt_contact1.text.toString()

        if(name == "" || name.length <=2 ){
            edt_name.error = getString(R.string.fields_text_condition)
            valid = false
        }
        if(firstname == "" || firstname.length <=2 ){
            edt_firstname.error = getString(R.string.fields_text_condition)
            valid = false
        }
        if ( !isEmailValid(email))
        {
            edt_email.error = getString(R.string.email_message)
            valid = false
        }
        if (id!! <=0){
            val radio = radio_masc.setError(getString(R.string.sexe_required))
            valid = false
        }
        if(txt_birth_date.text == getString(R.string.format_date))
        {
            txt_birth_date.error = getString(R.string.date_condition)
            valid = false
        }
        if (level == ""){
            valid = false
        }
        if (faculty == ""){
            valid = false
        }
        if (contact_1 == ""){
            edt_contact1.error = getString(R.string.contact_condition)
        }
        return valid
    }
    //Student save confirmation
    fun confirmation(){
        if (is_inputs_validate()){
            val builder = AlertDialog.Builder(this@AddAstudentActivity)
            builder.setTitle(getString(R.string.titre_confirmation))
            builder.setMessage(getString(R.string.confirmation_message)+" "+name.capitalize()+" ?")
            builder.setPositiveButton(getString(R.string.positive_response)){ dialog, which ->
                var student = Student(edt_name.text.toString(),  edt_firstname.text.toString(), edt_email.text.toString(), sp_niveau.selectedItem.toString(),
                    genre, sp_filiere.selectedItem.toString(), birth_date,  edt_contact1.text.toString(), edt_contact2!!.text.toString())

                val student_gson = Gson().toJson(student)
                startActivity(Intent(this@AddAstudentActivity, MainActivity::class.java).putExtra(MainActivity.STUDENT, student_gson))
                /*setResult(Activity.RESULT_OK, Intent(this@AddAstudentActivity, MainActivity::class.java).putExtra(MainActivity.STUDENT, student_gson))
                finish()*/
            }
            builder.setNegativeButton(getString(R.string.negative_response)){ dialog, which ->
                refresh_fields()
            }
            builder.setNeutralButton(getString(R.string.cancel)){ _, _ ->
                Toast.makeText(applicationContext,getString(R.string.cancel_message),Toast.LENGTH_SHORT).show()
            }
            val dialog = builder.create()
            dialog.show()
        }
    }
    //text input control
    fun setupFloatingLabelError(textInputLayout: TextInputLayout){
        textInputLayout.editText!!.addTextChangedListener(object: TextWatcher {
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().length <=2 ){
                    textInputLayout.error = getString(R.string.fields_text_condition)
                    textInputLayout.isErrorEnabled = true
                }else{
                    textInputLayout.isErrorEnabled = false
                }
            }
            override fun afterTextChanged(s: Editable?) {
            }
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }
        })
    }
    //email validation
    fun isEmailValid(email_parm: String):Boolean {
        return email_parm.isNotEmpty() && Patterns.EMAIL_ADDRESS.toRegex().matches(email_parm)
    }
    val dateSetListener = object : DatePickerDialog.OnDateSetListener{
        override fun onDateSet(view: DatePicker?, year: Int, month: Int, dayOfMonth: Int) {
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            formatDate()
        }
    }
    fun make_date_spicker(btn_date: Button){
        btn_date.setOnClickListener (object : View.OnClickListener {
            override fun onClick(view: View) {
                DatePickerDialog(this@AddAstudentActivity,
                    dateSetListener,
                    // set DatePickerDialog to point to today's date when it loads up
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show()
            }
        })
    }
    fun formatDate(){
        val format = getString(R.string.date_format2) // mention the format you need
        val simpleDateFormat = SimpleDateFormat(format, Locale.US)
        txt_birth_date.text = simpleDateFormat.format(calendar.time)
        birth_date = txt_birth_date.text.toString()
    }
    // Spinner Filières
    fun spinner_filiere() {
        sp_filiere.setOnItemSelectedListener(this)
        val array_adapter_filieres = ArrayAdapter(this, android.R.layout.simple_spinner_item, faculties)
        array_adapter_filieres.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        sp_filiere!!.setAdapter(array_adapter_filieres)
    }
    // Spinner Niveaux
    fun spinner_niveau(){
        sp_niveau.setOnItemSelectedListener(this)
        val array_adapter_niveaux = ArrayAdapter(this, android.R.layout.simple_spinner_item, levels)
        // Set layout to use when the list of choices appear
        array_adapter_niveaux.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        sp_niveau!!.setAdapter(array_adapter_niveaux)
    }
    override fun onNothingSelected(parent: AdapterView<*>?) {
        //To change body of created functions use File | Settings | File Templates.
    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        //To change body of created functions use File | Settings | File Templates.
        sp_niveau_msg.text = getString(R.string.selection) + levels.get(position)
        sp_filiere_msg.text = getString(R.string.selection) + faculties.get(position)
    }
    //refresh fields
    fun refresh_fields(){
        edt_name.text = null
        edt_firstname.text = null
        edt_email.text = null
        edt_contact1.text = null
        edt_contact2.text = null
        txt_birth_date.text = getString(R.string.format_date)
    }
}
