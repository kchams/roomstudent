package com.itplex.roomstudent.activities

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.google.gson.Gson
import com.itplex.roomstudent.R
import com.itplex.roomstudent.entity.Student

import kotlinx.android.synthetic.main.activity_student_detail.*
import kotlinx.android.synthetic.main.content_student_detail.*

class StudentDetailActivity : AppCompatActivity() {
    var student: Student? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_student_detail)
        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)


        if (intent.hasExtra(MainActivity.STUDENT)) {
            student = Gson().fromJson(intent.getStringExtra(MainActivity.STUDENT), Student::class.java)
            txt_name.text = student?.name
            txt_firstname.text = student?.first_name
            txt_email.text = student?.email
            txt_birth_date.text = student?.birth_date
            txt_contact1.text = student?.contact_1
            txt_contact2.text = student?.contact_2
            txt_level.text = student?.niveau
            txt_faculty.text = student?.faculty

            fab_email.setOnClickListener {
                startActivity(Intent(Intent.ACTION_SENDTO, Uri.fromParts("mailto", student?.email, null)))
            }
        }
        if(txt_contact2.text.isNotEmpty()){
            ic_contact2.setOnClickListener {
                startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:"+student?.contact_2)))
            }
        }
        fab.setOnClickListener { view ->
            startActivity(Intent(Intent.ACTION_CALL, Uri.parse("tel:"+student?.contact_1)))
        }
    }

}
