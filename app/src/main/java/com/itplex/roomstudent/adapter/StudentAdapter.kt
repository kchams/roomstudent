package com.itplex.roomstudent.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.AdapterView
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.itplex.roomstudent.R
import com.itplex.roomstudent.activities.MainActivity
import com.itplex.roomstudent.activities.StudentDetailActivity
import com.itplex.roomstudent.entity.Student
import kotlinx.android.synthetic.main.student_item_layout.view.*

class StudentAdapter(val context: Context): RecyclerView.Adapter<StudentAdapter.StudentViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var students = emptyList<Student>()
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentAdapter.StudentViewHolder {
        val itemView = inflater.inflate(R.layout.student_item_layout, parent, false)
        return StudentViewHolder(itemView)
    }

    class StudentViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val txtFullname = view.txt_fullname
        val txtLevel = view.txt_level
        val txtContact1 = view.txt_contact1
        val student = view.card_view
    }
    override fun onBindViewHolder(holder: StudentAdapter.StudentViewHolder, position: Int) {
        holder.txtFullname?.text = students.get(position).name+" "+students.get(position).first_name
        holder.txtLevel?.text = students.get(position).niveau
        holder.txtContact1?.text = students.get(position).contact_1
        holder.student?.setOnClickListener {
            context.startActivity(Intent(context, StudentDetailActivity::class.java)
                .putExtra(MainActivity.STUDENT, Gson().toJson((students.get(position)))) )
        }
    }
    internal fun setStudents(students: List<Student>) {
        this.students = students
        notifyDataSetChanged()
    }
    override fun getItemCount(): Int {
        return students.size
    }
}