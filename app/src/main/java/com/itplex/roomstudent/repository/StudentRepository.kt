package com.itplex.roomstudent.repository

import androidx.lifecycle.LiveData
import com.itplex.roomstudent.dao.StudentDao
import com.itplex.roomstudent.entity.Student


// Declares the DAO as a private property in the constructor. Pass in the DAO
// instead of the whole database, because you only need access to the DAO
class StudentRepository(private val studentDao: StudentDao) {

    // Room executes all queries on a separate thread.
    // Observed LiveData will notify the observer when the data has changed.
    val allStudents: LiveData<List<Student>> = studentDao.getAllStudent()

    suspend fun insert(student: Student) {
        studentDao.insert(student)
    }
}