package com.itplex.roomstudent.view_model

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import com.itplex.roomstudent.entity.Student
import com.itplex.roomstudent.repository.StudentRepository
import com.itplex.roomstudent.room_database.StudentRoomDatabase
import kotlinx.coroutines.launch

// Class extends AndroidViewModel and requires application as a parameter.
class StudentViewModel(application: Application) : AndroidViewModel(application) {

    // The ViewModel maintains a reference to the repository to get data.
    private val repository: StudentRepository
    // LiveData gives us updated words when they change.
    val allStudents: LiveData<List<Student>>

    init {
        // Gets reference to studentsDao from StudentRoomDatabase to construct
        // the correct WordRepository.
        val studentsDao = StudentRoomDatabase.getDatabase(application, viewModelScope).studentDao()
        repository = StudentRepository(studentsDao)
        allStudents = repository.allStudents
    }

    /**
     * The implementation of insert() in the database is completely hidden from the UI.
     * Room ensures that you're not doing any long running operations on
     * the main thread, blocking the UI, so we don't need to handle changing Dispatchers.
     * ViewModels have a coroutine scope based on their lifecycle called
     * viewModelScope which we can use here.
     */
    fun insert(student: Student) = viewModelScope.launch {
        repository.insert(student)
    }
}