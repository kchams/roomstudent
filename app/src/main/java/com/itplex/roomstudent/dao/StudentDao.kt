package com.itplex.roomstudent.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.itplex.roomstudent.entity.Student

@Dao
interface StudentDao {

    @Query("SELECT * from student_table ORDER BY name ASC")
    fun getAllStudent(): LiveData<List<Student>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(student: Student)

    @Query("DELETE FROM student_table")
    suspend fun deleteAll()
}